# Zelda Airline Company Client Profile Portal Backend

This project is aiming for contributing to the backend REST API design for Zelda Airline enterprise CRM profile portal.

Test cases:

* Create client profile
* Read the client profile being created with id specifid
* Update one or multiple fields of client's profile detail
* Delete the client profile with id specified

Due to the tight schedule, this project has not deployed any cloud service for storing the data. 
The profile data are stored in MongoDB running on your local machine, so please be sure that you have MongoDB installed and running before testing this project.

https://treehouse.github.io/installation-guides/mac/mongo-mac.html please refer to this site for more MongoDB installation reference.

The Mongoose Schema contains firstName, lastName, dob, email and homeAddress.

This project can be tested either on Postman or Swagger doc site, please check Running section for details. 


## Getting Started

These instructions below will get you a copy of the project up and running on your local machine for development and testing purposes.

From a terminal window, change to the local directory where you want to clone your repository.
Paste the command you copied from Bitbucket.
```
 $ git clone https://yyan3184@bitbucket.org/yyan3184/zelda-airline-profile-portal-backebnd.git
```
If the clone was successful, a new directory appears on your local drive. The directory is named 'zelda-airline-profile-portal-backebnd', please change directory into this folder.   




### Installing

Run the command to install all the local npm packages and dependencies.
```
 $ npm install
```

Once the installation completed, you will see a new folder 'node_modules' created under the directory.





### Running

Please be sure you have the MongoDB installed and running on your local device before proceeding.

Run the command to start the program.
```
 $ node start.js
```
Once it is started, please use Postman or Swagger Docs to run the test cases.


#### Postman running test cases


** Create client profile **

POST http://localhost:3000/profiles/create

x-www-form-urlencoded

BODY sample data: 

```
{
  "firstName": "Davis",
  "lastName": "Smith",
  "dob": "23/05/1975",
  "email": "davis.smith@hotmail.com",
  "homeAddress": "100 Georg St Sydney 2000 NSW"
}
```




** Read the client profile being created with id specifid **

GET http://localhost:3000/profiles/{id}

The ID you used in this step can be found at the profile creation successful message.




** Update one or multiple fields of client's profile detail **

PUT http://localhost:3000/profiles/{id}/update

x-www-form-urlencoded

BODY sample data: 

```
{
  "firstName": "David",
  "lastName": "Johnson",
  "dob": "11/11/1975",
  "email": "david.johnson@hotmail.com",
  "homeAddress": "100 Georg St Sydney 2000 NSW"
}
```




** Delete the client profile with id specified **

DELETE http://localhost:3000/profiles/{id}/delete



#### Swagger running test cases

http://localhost:3000/v1

Please visit this link in browser which will direct you to the Swagger Doc interface. For the configuration detail of Swagger in this application, please go to /dist/api-docs.json.


There will be multiple actions you can perform in the test. It has all the sample data/parameters available for testing with different types of real-time responses.

POST ​/profiles​/create

GET  ​/profiles​/{id}

PUT  ​/profiles​/{id}​/update

DELETE  ​/profiles​/{id}​/delete







## Logic / Coding Style

This project I have used the microservices pattern to serve the API requests. The application has been designed by MVC design pattern. 

M for models (mongoose schema), V for views which is not applicabale in this one, C for controllers which is the logic of how the app handles the incoming requests and outgoing responses.

There is one more module called Routes, Routes are our guide, they tell the client (browser/mobile app) to go to which Controller once a specific url/path is requested.



## Security Mechanism

Due to the limited time before my flight on this Sunday before Christmas, I am not able to provide implementation for the security mechanism.

I am thinking of integrating the API onto APIGEE Edge service, which enables more robust API management. i.e. verifying API keys at runtime, generating OAuth tokens, implementing JSON threat protection etc. 


