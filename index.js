import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
// import swagger from 'swagger-node-express';
var argv = require('minimist')(process.argv.slice(2));

import profile from './routes/profile.route'; // import routes for the profile



// mongodb database connection
const mongoDB = 'mongodb://localhost:27017/zelda';
mongoose.Promise = global.Promise;
mongoose.connect(mongoDB, { 
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
 });
const db = mongoose.connection;
db.on('error', () => {
    console.error.bind(console, 'MongoDB connection error:');
});
db.on('connected', () => {
    console.log('Mongoose default connection open to ' + mongoDB);
});
db.on('disconnected', () => {
    console.log('Mongoose default disconnected');
});
db.once('open', () => {
    console.log('db opened');
});



const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static('dist'));

app.use('/profiles', profile);




//swagger set to subpath to avoud route overlaps
const subpath = express();
app.use("/v1", subpath);
var swagger = require("swagger-node-express").createNew(subpath);
swagger.setApiInfo({
    title: "example API",
    description: "API to do something, manage something...",
    termsOfServiceUrl: "",
    contact: "yourname@something.com",
    license: "",
    licenseUrl: ""
});
subpath.get('/', function (req, res) {
    res.sendFile(__dirname + '/dist/index.html');
});
swagger.configureSwaggerPaths('', 'api-docs', '');
var domain = 'localhost';
if(argv.domain !== undefined){
    domain = argv.domain;
}
var applicationUrl = 'http://' + domain;
swagger.configure(applicationUrl, '1.0.0');





const port = 3000;
app.listen(port, () => {
  console.log(`Server running at port number :${port}`);
});