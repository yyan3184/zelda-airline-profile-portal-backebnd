import express from 'express';
const router = express.Router();

import profile_controller from '../controllers/profile.controller';

router.get('/test', profile_controller.test);

router.post('/create', profile_controller.profile_create);

router.get('/:id', profile_controller.profile_details);

router.put('/:id/update', profile_controller.profile_update);

router.delete('/:id/delete', profile_controller.profile_delete);

module.exports = router;
