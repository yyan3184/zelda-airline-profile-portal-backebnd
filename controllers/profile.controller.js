import Profile from '../models/profile.models';

exports.test = (req, res) => {
    res.send('Greetings from the test controller');
}

// create a new profile
exports.profile_create = (req, res) => {
    let profile = new Profile(
        {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            dob: req.body.dob,
            email: req.body.email,
            homeAddress: req.body.homeAddress
        }
    );

    profile.save((err, newProfile) => {
        if(err) {
            console.log('create profile error', err);
        }
        res.send(`New profile ${newProfile.id} has been reated successfully!`);
    });
}


// read the profile
exports.profile_details = (req, res) => {
    // console.log("reqqqqq",req);
    Profile.findById(req.params.id, (err, profile) => {
        if(err) {
            console.log('read profile error', err);
        }
        res.send(profile);
    });
}

//update the detail of profile
exports.profile_update = (req, res) => {
    Profile.findByIdAndUpdate(req.params.id, {$set: req.body}, (err, profile) => {
        if (err) {
            console.log('update profile error', err);
        }
        res.send('Profile has been udpated successfully!');
    });
};



//delete the profile
exports.profile_delete = (req, res) => {
    Profile.findByIdAndRemove(req.params.id, (err) => {
        if (err) {
            console.log('delete profile error', err);
        }
        res.send('Profile has been deleted successfully!');
    })
};

