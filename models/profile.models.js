import mongoose, { mongo } from 'mongoose';
const Schema = mongoose.Schema;

let ProfileSchema = new Schema({
    firstName: {type: String, required: true, max: 100},
    lastName: {type: String, required: true, max: 100},
    dob: {type: String, required: true},
    email: {type: String, required: true},
    homeAddress: {type: String, required: true}
});

module.exports = mongoose.model('Profile', ProfileSchema);